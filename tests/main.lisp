(defpackage enigma/tests/main
  (:use :cl
        :enigma
        :rove))
(in-package :enigma/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :enigma)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
