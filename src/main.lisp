(defpackage enigma
  (:use :cl :ringb))
(in-package :enigma)

;;;; Enigma Simulator by Michael Reis

#|
An engima machine is composed of a number of parts, a keyboard, a lightboard, a plugboard,
and several wheels. The keyboard puts power into the wires, which next passes through the plugboard
where it is mixed up in terms of inputs and outputs. Then it passes through several wheels which
are effectively rotary plugboards, though they aren't regularly changed in terms of internal wiring.
They do rotate though. And the signal then is turned around in another wheel that effectively
reflects it back into the wheel it came from. The signal then goes back through the plugboard
and into the light, illuminating the one that corrosponds to the cypher text, either encoding or
decoding based not on configuration of machine but only on the message being encoded or plain.
|#

;;; Objects
(defclass enigma ()
  ((keyboard   :initform (make-instance 'keyboard)   :reader keyboard)
   (plugboard  :initform (make-instance 'plugboard)  :reader plugboard)
   (wheel-0    :initform (make-instance 'wheel)      :reader wheel-0)
   (wheel-1    :initform (make-instance 'wheel)      :reader wheel-1)
   (wheel-2    :initform (make-instance 'wheel)      :reader wheel-2)
   (wheel-3    :initform (make-instance 'wheel)      :reader wheel-3)
   (reflector  :initform (make-instance 'reflector)  :reader reflector)
   (lightboard :initform (make-instance 'lightboard) :reader lightboard))
  (:documentation
   "The full naval version of the enigma machine has a plugboard and four wheels that, as well as
   the reflector, rotate."))
(defclass single-wheel-enigma (enigma)
  ((wheel-1 :initform nil)
   (wheel-2 :initform nil)
   (wheel-3 :initform nil))
  (:documentation
   "This version isn't real, I made it for testing, it is the full version in terms of types,
   but only has one wheel."))
(defclass board ()
  ((state :initform (make-ringb 26) :reader state)
   (plugs :initform (fill-ringb-ordered (make-ringb 26)) :reader plugs))
  (:documentation
   "Boards have state, a bit-vector where each cell in the vector represents one wire
   and the bit at that index is the status of the wire, powered or not. Boards have an int-vector
   where the index of each cell in the vector represents one input. The state will be updated
   according to which integers, are = to eachother between boards."))
(defclass lightboard (board) ()
  (:documentation
   "The lightboard is the start and end to each wire before the keyboard disconnects one line to
   take it over and inject the input signal."))
(defclass keyboard (board)
  ((pressed :initform 0 :accessor pressed))
  (:documentation
   "The keyboard inputs the number to the system while also blocking the lightboard from lighting
   up any of the keys inputted. The plugboard puts power into the system, changing the state,
  which propogates through the plugboards and back to the lightboard."))
(defclass plugboard (board) ()
  (:documentation
   "The plugboard has wires that redirect the signals, effectively swapping them. Depenging on which
   of the two signals is powered, you get the signal at the other output."))
(defclass wheel (board) ()
  ((notches :initform (make-ringb 26)))
  (:documentation
   "A wheel is a type of board that can rotate. The rotation is virtualized by using an index
   to track the amount of offset needed to locate where the wires should get their input from.
   In the wheel pack, the 0th wheel steps every key-stroke, and each successive wheel steps once
   per full turn of the previous wheel. Wheels have notches which are used for alignment of the
   wheels to one another for the system to determine when to turn them."))
(defclass reflector (wheel)
  ((plugs :initform (fill-ringb-reversed (make-ringb 26))))
  (:documentation
   "The reflector is a unique type of wheel that only has half the wires, while having a full
   number of inputs, the output is just another input."))

;;; Printing
(defun print-enigma (enigma)
  (print-object enigma t))
(defmethod print-object ((enigma enigma) (stream t))
  (format stream "ENIGMA: ~%")
  (loop for part in (list ; TODO this information should be available to lookup rather than explicitly listed
                     (keyboard enigma) (lightboard enigma) (plugboard enigma)
                     (wheel-0 enigma) (wheel-1 enigma) (wheel-2 enigma) (wheel-3 enigma)
                     (reflector enigma))
        for part-name in '(keyboard lightboard plugboard wheel-0 wheel-1 wheel-2 wheel-3 ; TODO this also should be available by lookup rather than listed
                           reflector)
        do (format stream "~&~10A: ~A~&"
                   part-name (when part (state part)))))
(defmethod print-object ((board board) (stream t))
  (format stream "TYPE: ~A~&STATE: ~A~&PLUGS: ~A~&"
          (type-of board) (state board) (plugs board)))
(defmethod print-object ((wheel wheel) (stream t))
  (format stream "TYPE: ~A~&STATE: ~A~&PLUGS: ~A~&"
          (type-of wheel) (state wheel) (plugs wheel)))
(defmethod print-object ((keyboard keyboard) (stream t))
  (format stream "TYPE: ~A~&PRESSED-KEYS: ~A~&STATE: ~A~&PLUGS:~A~&"
          (type-of keyboard) (pressed keyboard) (state keyboard) (plugs keyboard)))
(defun get-lightboard-char-list (enigma)
  (loop for index from 0 below 26
        when (plusp (get-state (lightboard enigma) index))
          collecting (code-alph index) into result
        finally (return result)))
(defun newline-maybe (&optional (stream t))
 (format stream "~&"))

;;; Code to char and back
(defun alph-code (alph)
  (assert (alpha-char-p alph))
  (let ((map (make-alph-code-map)))
    (rest (assoc (char-code alph) map :test #'=))))
(defun make-alph-code-map ()
  (loop for code from 0 below 26
        for low from 97 to 122
        for high from 65 to 90
        collecting (cons low code) into lows
        collecting (cons high code) into highs
        finally (return (append lows highs))))
(defun code-alph (code)
  (assert (integerp code))
  (let ((map (make-code-alph-map)))
    (rest (assoc code map :test #'=))))
(defun make-code-alph-map ()
  (loop for code from 0 below 26
        for high from 65 to 90
        collecting (cons code (code-char high))))

;;; Initialization
(defun fill-ringb-ordered (ringb)
  (list-to-ringb (loop for index from 0 below 26 collecting index) ringb)
  ringb)
(defun fill-ringb-reversed (ringb)
  (list-to-ringb (loop for index from 25 downto 0 collecting index) ringb)
  ringb)
(defun fill-ringb-random (ringb)
  (list-to-ringb (make-random-ints-list 0 26) ringb)
  ringb)

;;; Randomizing utils
(defun make-random-ints-list (low high)
  "Make a list of random numbers from 0 below count (exclusive), using seed to shuffle
  pseudo-randomly."
    (loop with result = '()
          for index from low
          for next = (next-random high)
          while (< (length result) high)
          when (not (find next result))
            do (push next result)
          finally (return result)))
(defun make-random-pairs-list (low high)
  (loop with result = '()
        with prev = nil
        for entry in (make-random-ints-list low high)
        do (if (not prev)
               (setf prev entry)
               (progn (push (cons prev entry) result)
                      (setf prev nil)))
        finally (return (nreverse result))))
(let* ((state (make-random-state nil))
       (copy (make-random-state state)))
  (defun reset-state () ; TODO save state to disk
    "Set the state back to the last version which was set when this was compiled."
    (setf copy (make-random-state state))
    t)
  (defun next-random (max)
    "Using the saved state, call random as usual."
   (random max copy))) 

;;; Randomization
(defgeneric randomize-enigma (enigma))
(defmethod randomize-enigma ((enigma enigma))
  (randomize (plugboard enigma))
  (randomize (wheel-0 enigma))
  (randomize (wheel-1 enigma))
  (randomize (wheel-2 enigma))
  (randomize (wheel-3 enigma))
  (randomize (reflector enigma)))
(defmethod randomize-enigma ((enigma single-wheel-enigma))
  (randomize (plugboard enigma))
  (randomize (wheel-0 enigma))
  (randomize (reflector enigma)))
(defgeneric randomize (board))
(defmethod randomize ((board board))
  (loop with new-order = (make-random-ints-list 0 26)
        for index from 0 below 26
        do (setf (get-raw-plug board index) (first new-order)
                 new-order (rest new-order)))
  board)
(defmethod randomize ((reflector reflector))
  (loop for (left . right) in (make-random-pairs-list 0 26)
        do (setf (get-raw-plug reflector left) right
                 (get-raw-plug reflector right) left))
  reflector)

;;; Object access
(defun get-raw-plug (board index)
  (ringb-entry (plugs board) index))
(defun (setf get-raw-plug) (new board index)
  (setf (ringb-entry (plugs board) index) new))
(defun get-plug (board index)
  (offset-entry (plugs board) index))
(defun (setf get-plug) (new board index)
  (setf (offset-entry (plugs board) index) new))
(defun get-state (board index)
  (ringb-entry (state board) index))
(defun (setf get-state) (new board index)
  (setf (ringb-entry (state board) index) new))

;;; Key presses and releases
(defun press-key (enigma char)
  (when (zerop (get-state (keyboard enigma) (alph-code char)))
    (step-all-wheels enigma))
  (setf (get-state (keyboard enigma) (alph-code char)) 1)
  (incf (pressed (keyboard enigma))))
(defun release-key (enigma char)
  (when (plusp (get-state (keyboard enigma) (alph-code char)))
    (setf (get-state (keyboard enigma) (alph-code char)) 0)
    (decf (pressed (keyboard enigma)))))
(defun release-all-keys (enigma)
  (loop for index from 0 below 26
        do (setf (get-state (keyboard enigma) index) 0))
  (setf (pressed (keyboard enigma)) 0))
;;; State propogation
(defun update-state-fwd (prev next)
  "Propogate the state from the prev to the next and back following the randomization of
  the plugs and taking into account the offset of the wheels."
  (loop for prev-index from 0 below 26
        for next-index = (get-plug prev prev-index)
        for state-prev = (get-state prev prev-index)
        do
           ;; (format t "~10A at ~2A to ~10A at ~2A: ~1A~%"
           ;;         (type-of prev) prev-index (type-of next) next-index state-prev)))
           (setf (get-state next next-index) state-prev)))
(defun update-state-rev (prev next)
  "Update the state in the backwards order, accounting for plug wiring and wheel offset."
  (loop for next-index from 0 below 26
        for prev-index = (get-plug next next-index)
        for state-prev = (get-state prev prev-index)
        for state-next = (get-state next next-index)
        do
           ;; (format t "~10A at ~2A to ~10A at ~2A: ~1A~%"
           ;;         (type-of prev) prev-index (type-of next) next-index state-prev)))
           (setf (get-state next next-index) state-prev)))
(defun update-state-reflector (reflector)
  (loop for index from 0 below 26
        for plug = (get-plug reflector index)
        for state = (get-state reflector index)
        when (plusp state)
          do
             ;; (format t "~10A at ~2A to ~2A: ~1A~%"
             ;;         (type-of reflector) index plug state)
             (setf (get-state reflector plug) 1)))
(defun update-state-lightboard (lightboard keyboard)
  (loop for index from 0 below 26
        for state-lightboard = (get-state lightboard index)
        for state-keyboard = (get-state keyboard index)
        when (and (plusp state-lightboard) (plusp state-keyboard))
          do
             ;; (format t "~10A at ~2A: key = light ~%"
             ;;         (type-of lightboard) index)
             (setf (get-state lightboard index) 0)))
(defgeneric update-states (enigma))
(defmethod update-states ((enigma enigma))
  (update-state-fwd (keyboard enigma)  (plugboard enigma))
  (update-state-fwd (plugboard enigma) (wheel-0 enigma))
  (update-state-fwd (wheel-0 enigma)   (wheel-1 enigma))
  (update-state-fwd (wheel-1 enigma)   (wheel-2 enigma))
  (update-state-fwd (wheel-2 enigma)   (wheel-3 enigma))
  (update-state-fwd (wheel-3 enigma)   (reflector enigma))
  (update-state-reflector (reflector enigma))
  (update-state-rev (reflector enigma) (wheel-3 enigma)) 
  (update-state-rev (wheel-3 enigma)   (wheel-2 enigma))
  (update-state-rev (wheel-2 enigma)   (wheel-1 enigma))
  (update-state-rev (wheel-1 enigma)   (wheel-0 enigma))
  (update-state-rev (wheel-0 enigma)   (plugboard enigma))
  (update-state-rev (plugboard enigma) (lightboard enigma))
  (update-state-lightboard (lightboard enigma) (keyboard enigma))
  enigma)
(defmethod update-states ((enigma single-wheel-enigma))
  (update-state-fwd (keyboard enigma)  (plugboard enigma))
  (update-state-fwd (plugboard enigma) (wheel-0 enigma))
  (update-state-fwd (wheel-0 enigma)   (reflector enigma))
  (update-state-reflector (reflector enigma))
  (update-state-rev (reflector enigma) (wheel-0 enigma))
  (update-state-rev (wheel-0 enigma)   (plugboard enigma))
  (update-state-rev (plugboard enigma) (lightboard enigma))
  (update-state-lightboard (lightboard enigma) (keyboard enigma))
  enigma)

;;; Wheel index updates ; TODO make this work with notches
(defun step-all-wheels (enigma)
  (step-wheel (wheel-0 enigma))
  (when (zerop (wheel-offset (wheel-0 enigma)))
    (step-wheel (wheel-1 enigma))
    (when (zerop (wheel-offset (wheel-1 enigma)))
      (step-wheel (wheel-2 enigma))
      (when (zerop (wheel-offset (wheel-2 enigma)))
        (step-wheel (wheel-3 enigma))
        (when (zerop (wheel-offset (wheel-3 enigma)))
          (step-wheel (reflector enigma)))))))
(defun step-wheel (wheel)
  (next-offset (plugs wheel))
  (next-offset (state wheel)))
(defun wheel-offset (wheel)
  (ringb-offset (plugs wheel)))

;;; High level functionality
(defun transcode-char (enigma char)
  "Encode or decode the character using the enigma. The char must be an alphabetic character."
  (format t "~&CRYPT IN: ~A  " (char-upcase char))
  (press-key enigma char)
  (update-states enigma)
  (format t "CRYPT OUT: ~A~&" (first (get-lightboard-char-list enigma))) ; TODO this only transcodes one char at a time, probably need some error handling to ensure system is ready for the next input
  (release-key enigma char)
  (update-states enigma))
(defun transcode-alpha-string (enigma string)
  "Encode or decode the string using the enigma. The string must only contain alphabetic characters."
  (loop for char across string
        do (transcode-char enigma char)))
(defun transcode-string (enigma string)
  "Encode or decode the string using the enigma. If the string contains non-alphabetic characters,
  they will be dropped."
  (loop for char across string
        when (alpha-char-p char)
          do (transcode-char enigma char)))
;; TODO specify wheel layout
;; TODO set wheel indexing


;;; Tests
(defun test ()
  (let ((enigma (make-instance 'enigma)))
    (reset-state)
    (randomize-enigma enigma)
    (loop for test from 0 below 10 do (transcode-char enigma #\c))
    (transcode enigma "testonetwothreetest")
    (encode-english enigma "test one, two, 3; test.")))
