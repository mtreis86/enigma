;;;; Enigma Simulator by Michael Reis


;;; Parts are connected from the index to the contents of a vector, index is the input number,
;;; contents is the output number. Updating the machine is to follow the parts and adjust the state
;;; according to the contents of these positions.

(defclass part ()
  ((name :type string :initarg :name :accessor name)
   (state :type bit-vector
          :initform (make-array 26 :initial-element 0 :element-type 'bit)
          :accessor state)))

(defclass wired-part ()
  ((wiring :type vector
           :initform (make-array 26 :initial-contents (loop for x from 0 below 26 collecting x))
           :reader wiring))
  (:documentation "A wired part has a wiring map which consists of a vector. The index of each cell
 is the input and the content is the wire's output. Wires have no state but are used to map out
 state changes in the enigma system."))

(defclass lightboard (wired-part)
  ()) ; TODO?

(defclass keyboard (part)
  ()) ; TODO?

(defclass plugboard (wired-part)
  ()) ; TODO?

(defclass wheel (plugboard)
  ()) ; TODO?

(defclass reflector (wheel)
  ()) ; TODO?


(defgeneric update-state (part &optional options))

(defmethod update-state ((part part) &optional (options nil))
  (declare (ignore options))
  (let ((input-parts (input-parts part)))
    (process part input-parts)))

(defun input-parts (part)
  (declare (ignore part))
  (error "not written yet")) ; TODO

(defun process (part inputs)
  (declare (ignore part inputs))
  (error "not written yet")) ; TODO

(defmethod update-state ((keyboard keyboard) &optional code)
  "Reset the keyboard then update the state to having the bit at the code turned on."
  (let ((keyboard (reset-keyboard keyboard)))
    (set-state keyboard code 1)))

(defun reset-keyboard (keyboard)
  "Reset all the keys to off."
  (loop for state across (state keyboard)
        for index from 0
        do (set-state keyboard index 0)))

(defun set-state (part index bit)
  "Set the bit at the index in the state of the part to 1 or 0 depending on bit."
  (declare (type bit bit))
  (setf (aref (state part) index) bit))

(defun press-key (char keyboard)
  "Set the keyboard based on the the Lisp character."
  (let ((code (char-code char)))
    (if (or (not (alpha-char-p char)) (> code 71))
        (error "only 26 keys")
        (update-state keyboard (if (< code 26)
                                   (print code)
                                   (- code 26))))
    keyboard))

(defun wire-plugs (plugboard &rest new-wires)
  "Set the plugboard to have the wires all connected."
  (declare (ignore plugboard new-wires))
  '()) ; TODO






