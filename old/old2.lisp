(defpackage enigma
  (:use :cl))
(in-package :enigma)

;;;; Enigma Simulator by Michael Reis

#|

Different way to think about this, I have a bit vector that gets passed around functions named
for the part the simulate. Since I am not going for speed over debugability I will make a new
vector at each function.

|#

(defun make-byte-vector (bits-per-byte length)
  (make-array length :element-type 'bit-vector :initial-element (make-bit-vector bits-per-byte)))

(defun lower-case (char)
  (let ((code (char-code char)))
    (code-char
     (cond ((or (< code 65) ; less than A
                (and (> code 90) (< code 97)) ; more than Z but less than a
                (> code 122)) ; more than z
            (error "out of range"))
           ((< code 91)
            (+ code 32))
           (t code)))))

(defstruct (baudot (:print-function print-baudot))
  char code)

(defun print-baudot (baudot &optional (stream t) depth)
  (declare (ignore depth))
  (format stream "~A : ~A  " (baudot-char baudot) (baudot-code baudot)))

(defun make-keys ()
  (make-array 26 :element-type 'baudot :initial-contents 
              (loop for code from 0 to 25
                    for char = (code-char (+ code 65))
                    collecting (make-baudot :char char :code (int-to-bit-vector code 5)))))
  


