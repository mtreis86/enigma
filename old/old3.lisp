(defpackage enigma
  (:use :cl :ringb))
(in-package :enigma)

;;;; Enigma Simulator by Michael Reis

#|

|#




(defstruct (enigma     (:constructor %make-enigma%) (:print-function print-enigma))
  keyboard plugboard wheel-0 wheel-1 wheel-2 wheel-3 reflector lightboard)

;; Boards have state, a bit-vector where each cell in the vector represents one wire
;; and the bit at that index is the status of the wire, powered or not.
(defstruct (board      (:constructor %make-board%))                            state) ; TODO boards and plugboards can probably be combined, no need to superclass them when there are no branches
;; Plugboards have a int-vector where the index of each cell in the vector represents one input
;; and the contents of the cell the output. Plugboards are interconnected output to input.
;; The state will be updated according to which integers, are = to eachother between plugboards.
(defstruct (plugboard  (:constructor %make-plugboard%)  (:include board))      plugs)
;; The keyboard inputs the number to the system while also blocking the lightboard from lighting
;; up any of the keys inputted. The plugboard puts power into the system, changing the state,
;; which propogates through the plugboards and back to the lightboard.
(defstruct (keyboard   (:constructor %make-keyboard%)   (:include plugboard)))
;; The lightboard is the start and end to each wire before the keyboard disconnects one line to
;; take it over and inject the input signal.
(defstruct (lightboard (:constructor %make-lightboard%) (:include plugboard)))
;; A wheel is a type of plugboard that can rotate. The rotation is virtualized by using an index
;; to track the amount of offset needed to locate where the wires should get their input from.
;; In the wheel pack, the 0th wheel steps every key-stroke, and each successive wheel steps once
;; per full turn of the previous wheel.
(defstruct (wheel      (:constructor %make-wheel%)      (:include plugboard))  index)
;; The reflector is a unique type of wheel that only has half the wires, while having a full
;; number of inputs, the output is just another input.
(defstruct (reflector  (:constructor %make-reflector%)  (:include wheel)))

(defun make-enigma ()
  (%make-enigma%
   :keyboard (make-keyboard)
   :plugboard (make-plugboard)
   :wheel-0 (make-wheel)
   :wheel-1 (make-wheel)
   :wheel-2 (make-wheel)
   :wheel-3 (make-wheel)
   :reflector (make-reflector)
   :lightboard (make-lightboard)))
(defun make-keyboard ()
  (%make-keyboard%
   :plugs (make-int-vector 26)
   :state (make-bit-vector 26)))

(defun make-plugboard ()
  (%make-plugboard%
   :plugs (make-int-vector 26)
   :state (make-bit-vector 26)))

(defun make-wheel ()
  (%make-wheel%
   :plugs (make-int-vector 26)
   :state (make-bit-vector 26)
   :index 0))

(defun make-reflector ()
  (%make-reflector%
   :plugs (make-flipped-int-vector 26)
   :state (make-bit-vector 26)
   :index 0))

(defun make-lightboard ()
  (%make-lightboard%
   :plugs (make-int-vector 26)
   :state (make-bit-vector 26)))

(defun get-plug (plugboard index)
  (aref (plugboard-plugs plugboard) index))

(defun (setf get-plug) (new plugboard index)
  (setf (aref (plugboard-plugs plugboard) index) new))

(defun press-key (enigma key-int)
  (set-state (enigma-keyboard enigma) key-int 1))


;;; Updating the state is where the effects of the wiring is captured
(defun update-states (enigma) ; TODO refactor this into a loop or something easier to edit or more responsive to changes
  (update-state-plug       (enigma-keyboard enigma)  (enigma-plugboard enigma))
  (update-state-plug       (enigma-plugboard enigma) (enigma-wheel-0 enigma))     ; first psi rotates every update
  (update-state-wheel      (enigma-wheel-0 enigma)   (enigma-wheel-1 enigma))     ; other psis each rotate like clocks per rotation of the previous
  (update-state-wheel      (enigma-wheel-1 enigma)   (enigma-wheel-2 enigma))
  (update-state-wheel      (enigma-wheel-2 enigma)   (enigma-wheel-3 enigma))
  (update-state-wheel      (enigma-wheel-3 enigma)   (enigma-reflector enigma)))   ; reflector rotates every update
  ;; (update-state-reflector  (enigma-reflector enigma))                             ; reflector reflects
  ;; (update-state-rev        (enigma-reflector enigma) (enigma-wheel-3 enigma))
  ;; (update-state-rev        (enigma-wheel-3 enigma)   (enigma-wheel-2 enigma))
  ;; (update-state-rev        (enigma-wheel-2 enigma)   (enigma-wheel-1 enigma))
  ;; (update-state-rev        (enigma-wheel-1 enigma)   (enigma-wheel-0 enigma))
  ;; (update-state-rev        (enigma-wheel-0 enigma)   (enigma-plugboard enigma))
  ;; (update-state-plug       (enigma-plugboard enigma) (enigma-lightboard enigma))
  ;; (update-state-lightboard (enigma-keyboard enigma)  (enigma-lightboard enigma))) ; lightboard and keyboard with the same state produce a zero

;;; TODO what order to psi and chi wheels turn? What order are they in electrically?
;;; I currently have psi advancing once every step and chi every other step, and psi located before
;;; chi and both arranged in order the indexes advance in. I think this is wrong and
;;; will need to look into it. When did the reflector rotate?

(defun update-state-plug (from into)
  (loop for state across (board-state from)
        for index across (plugboard-plugs from)
        do (set-state into index state)))

(defun update-state-wheel (from into)
  (loop for state across (board-state from)
        for from-index across (plugboard-plugs from)
        do (set-state into (mod (+ from-index (wheel-index into)) 26) state)))    

(defun update-state-rev (from into)
  (loop for into-index from 0 below 26
        for from-index = (mod (+ into-index (wheel-index from)) 26)
        for state = (aref (board-state from) from-index)
        do (setf (aref (board-state into) into-index) state)))

(defun update-indexes (enigma)
  (incf (wheel-index (enigma-wheel-0 enigma)))          ; wheel-0 
  (let ((index (wheel-index (enigma-wheel-0 enigma))))                          
    (when (zerop (mod index 26))
      (incf (wheel-index (enigma-wheel-1 enigma))))     ; wheel-1
    (when (zerop (mod index (expt 26 2)))
      (incf (wheel-index (enigma-wheel-2 enigma))))     ; wheel-2
    (when (zerop (mod index (expt 26 3)))
      (incf (wheel-index (enigma-wheel-3 enigma))))     ; wheel-3
    (when (zerop (mod index (expt 26 4)))
      (incf (wheel-index (enigma-reflector enigma)))))) ; reflector

(defun update-state-reflector (reflector)
  (loop for idx from 0 below 26
        for idy = (aref (plugboard-plugs reflector) idx)
        for statex = (get-state reflector idx)
        when (plusp statex)
          do (set-state reflector idy 1)))

(defun update-state-lightboard (keyboard lightboard)
  (loop for index from 0 below 26
        for key-state across (board-state keyboard)
        for light-state across (board-state lightboard)
        when (= 1 key-state light-state)
          do (set-state lightboard index 0)))

(defun release-keys (enigma)
  (loop for index from 0 below 26
        do (set-state (enigma-keyboard enigma) index 0))
  (update-states enigma))

(defun toggle-key (keyboard key-int)
  (set-state keyboard key-int (flip-state (get-state keyboard key-int))))

(defun flip-state (bit)
  (cond ((= bit 0) 1)
        ((= bit 1) 0)
        (t (error "~A is not a bit" bit))))

(defun get-state (board index)
  (aref (board-state board) index))

(defun set-state (board index bit)
  "Set the bit at index in the state to bit."
  (setf (aref (board-state board) index) bit))


(defun make-bit-vector (length)
  (make-array length :element-type 'bit :adjustable nil :fill-pointer nil :initial-element 0))

(defun make-int-vector (length)
  (make-array length :element-type 'integer :adjustable nil :fill-pointer nil
                     :initial-contents (loop for index from 0 below length collecting index)))

(defun make-flipped-int-vector (length)
  (make-array length :element-type 'integer :adjustable nil :fill-pointer nil
                     :initial-contents (loop for index downfrom (1- length) to 0 collecting index)))

(defun print-lightboard (lightboard)
  (loop for state across (board-state lightboard)
        for index from 0
        initially (format t "enabled states: ")
        when (plusp state) do (format t "~A " (code-alph index))
        finally (terpri)))

(defun randomize-enigma (enigma)
  ;; (randomize-plugboard (enigma-plugboard enigma))
  ;; (randomize-plugboard (enigma-wheel-0 enigma))
  ;; (randomize-plugboard (enigma-wheel-1 enigma))
  (randomize-plugboard (enigma-wheel-2 enigma))
  ;; (randomize-plugboard (enigma-wheel-3 enigma))
  (randomize-reflector (enigma-reflector enigma)))

(defun randomize-plugboard (plugboard)
  (let ((new-order (make-random-ints-list 26)))
    (loop for index from 0 below 26
          for next = (first new-order)
          do (setf (get-plug plugboard index) next)
             (setf new-order (rest new-order)))
    plugboard))

(defun randomize-reflector (reflector)
  (let ((new-order (make-random-pairs-list 26)))
    (loop for (left . right) in new-order
          do (setf (get-plug reflector left) right
                   (get-plug reflector right) left))
    reflector))
 
(defun make-random-ints-list (count)
  "Make a list of random numbers from 0 below count (exclusive), using seed to shuffle
  pseudo-randomly."
    (loop with result = '()
          for index from 0
          for next = (next-random count)
          while (< (length result) count)
          when (not (find next result))
            do (push next result)
          finally (return result)))

(defun make-random-pairs-list (count)
  (loop with result = '()
        with prev = nil
        for entry in (make-random-ints-list count)
        do (if (not prev)
               (setf prev entry)
               (progn (push (cons prev entry) result)
                      (setf prev nil)))
        finally (return (nreverse result))))

(let* ((state (make-random-state nil))
       (copy (make-random-state state)))
  (defun reset-state () ; TODO save state to disk
    "Set the state back to the last version which was set when this was compiled."
    (setf copy (make-random-state state))
    t)
  (defun next-random (max)
    "Using the saved state, call random as usual."
    (random max copy)))

(defun print-enigma (enigma &optional (stream t) depth)
  (declare (ignore depth))
  (print-object enigma stream))

(defmethod print-object ((enigma enigma) (stream t))
  (format stream "New enigma: ~%")
  (loop for part in (list ; TODO this information should be available to lookup rather than explicitly listed
                     (enigma-keyboard enigma)  (enigma-plugboard enigma) (enigma-wheel-0 enigma)
                     (enigma-wheel-1 enigma)   (enigma-wheel-2 enigma)   (enigma-wheel-3 enigma)
                     (enigma-reflector enigma) (enigma-lightboard enigma))
        for part-name in '(keyboard plugboard wheel-0 wheel-1 wheel-2 wheel-3 ; TODO this also should be available by lookup rather than listed
                           reflector lightboard)
        do (format stream "~10A: ~A   INDEX : ~A~%" part-name (board-state part)
                   (when (typep part 'wheel) (wheel-index part)))))

(defmethod print-object ((plugboard plugboard) (stream t))
  (format stream "TYPE: ~A INDEX: ~A~%BOARD-STATE: ~A  PLUGBOARD-PLUGS: ~A~%"
          (type-of plugboard)     (when (typep plugboard 'wheel) (wheel-index plugboard))
          (board-state plugboard) (plugboard-plugs plugboard)))

(defun alph-code (alph)
  (assert (alpha-char-p alph))
  (let ((map (make-alph-code-map)))
    (rest (assoc (char-code alph) map :test #'=))))

(defun make-alph-code-map ()
  (loop for code from 0 below 26
        for low from 97 to 122
        for high from 65 to 90
        collecting (cons low code) into lows
        collecting (cons high code) into highs
        finally (return (append lows highs))))

(defun code-alph (code)
  (assert (integerp code))
  (let ((map (make-code-alph-map)))
    (rest (assoc code map :test #'=))))

(defun make-code-alph-map ()
  (loop for code from 0 below 26
        for high from 65 to 90
        collecting (cons code (code-char high))))










(defun test ()
  (let ((enigma (make-enigma)))
    (randomize-enigma enigma)
    (test-key enigma 5)
    enigma))

(defun test-key (enigma key)
  (format t "Testing key: ~A~%" (code-alph key))
  (print-lightboard (enigma-lightboard enigma))
  (press-key enigma key)
 ; (update-indexes enigma)
  (update-states enigma)
  (print-object enigma t)
  (print-lightboard (enigma-lightboard enigma))
  (terpri))

