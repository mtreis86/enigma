(defsystem "enigma"
  :version "0.1.0"
  :author "Michael Reis"
  :license "All Rights Reserved"
  :depends-on ("ringb")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "enigma/tests"))))

(defsystem "enigma/tests"
  :author "Michael Reis"
  :license "All Rights Reserved"
  :depends-on ("enigma"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for enigma"
  :perform (test-op (op c) (symbol-call :rove :run c)))
